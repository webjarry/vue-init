import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import './utils/rem';

/* 引入第三方插件 */
import "./utils/init";

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
