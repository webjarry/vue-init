import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import {Toast} from "vant";
import storage from "../utils/storage";

Vue.use(VueRouter);

const LoginViews = () => import( "../views/Sign/Login.vue" );
const SignViews = () => import( "../views/Sign.vue" );

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
        meta: {
            isLogin: false
        }
    },
    {
        path: "/sign",
        redirect: "/sign/login",
        component: SignViews,
        children: [
            {
                path: "/sign/login",
                name: "Login",
                component: LoginViews
            }
        ]
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    let AccountToken = storage.get('AccountToken');

    !to.meta.isLogin ?
        next() : AccountToken !== null && AccountToken.length >= 10 ?
        next() : Toast.fail('请先登录!') && next({name: 'Login'})

});

export default router;
