import Vue from 'vue'
import '../assets/less/Components.less'
import * as custom from '../utils/filters'

import VantUI from 'vant'
import 'vant/lib/index.css'

import Touch from 'vue-touch'

Object.keys(custom).forEach(key => {
    Vue.filter(key, custom[key]);
});
Vue.use(Touch);
Vue.use(VantUI);
