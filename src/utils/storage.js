let storage = {
    set: (key, value) => {
        let item = typeof value === 'string' ? value : typeof value === 'object' ? JSON.stringify(value) : value;

        return localStorage.setItem(key, item);
    },
    get: (key) => {
        let value = localStorage.getItem(key);

        return value;
    },
    remove: (key) => {
        return localStorage.removeItem(key);
    }
};

export default storage