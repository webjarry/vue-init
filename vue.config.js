module.exports = {
    devServer: {
        open: true,

        proxy: {
            '/juh': {
                target: 'http://apis.juhe.cn',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/juh': ''
                }
            },
            '/api': {
                target: 'http://47.101.29.248:9000',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    }
};